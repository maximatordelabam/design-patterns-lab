/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.sensor.ValeurC;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Maxime
 */
public class CommandeSetUnitC implements Commande{
    
    private ISensor sensor;
    
    public CommandeSetUnitC(ISensor s){
        
        this.sensor = s;
        
    }
    
    public ISensor execute(){
        
         try {
            while(sensor.getValueType() != new ValeurC(sensor).getName()){
            sensor.changeUnite();
            }
        } catch (SensorNotActivatedException ex) {
            Logger.getLogger(CommandeSetUnitCarr.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sensor;
        
    }
    
    
}
