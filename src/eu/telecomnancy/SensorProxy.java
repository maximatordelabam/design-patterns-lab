/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import java.util.Observer;
import javafx.beans.Observable;

/**
 *
 * @author hubert52u
 
public class SensorProxy extends Observable implements ISensor {

    ISensor sensor;    
    
    public SensorProxy(ISensor s){
        this.sensor = s;
    }
    
    public ISensor getSensor(){
        return this.sensor;
    }
    
    
    @Override
    public void on(){
        this.sensor.on();
    }

    @Override
    public void off() {
        this.sensor.off();
    }

    @Override
    public boolean getStatus() {
        return this.sensor.getStatus();
    }

    @Override
    public void update() throws SensorNotActivatedException {
        this.sensor.update();

    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        
        return this.sensor.getValue();
    }

    public void update(java.util.Observable o, Object arg) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   


    
}
*/