package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorProxy;
import eu.telecomnancy.sensor.SimpleSensorLogger;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.ui.MainWindow;

public class SwingApp {

    public static void main(String[] args) {
        ISensor sensor = new TemperatureSensor();
    	SensorProxy psensor = new SensorProxy(sensor, new SimpleSensorLogger());
        new MainWindow(sensor);
    }

}
