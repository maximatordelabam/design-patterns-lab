package eu.telecomnancy.sensor;

import java.util.Observable;
import java.util.Date;



public class SensorProxy extends Observable implements ISensor {
	
	private ISensor sensor ;
	private SensorLogger log;
	
	public SensorProxy(ISensor s, SensorLogger sensorlogger) {
		this.sensor = s;
		this.log = sensorlogger;
	}

	@Override
	public void on() {
		// TODO Auto-generated method stub
		this.sensor.on();
		//System.out.println(new Date().getTime());
		//System.out.println("Méthode appelée : public void on()");
		log.log(LogLevel.INFO, "Sensor On");
		
			}

	@Override
	public void off() {
		// TODO Auto-generated method stub
		this.sensor.off();
		//System.out.println(new Date().getTime());
		//System.out.println("Méthode appelée : public void off()");
		log.log(LogLevel.INFO, "Sensor Off");
		
	}

	@Override
	public boolean getStatus() {
		// TODO Auto-generated method stub
		log.log(LogLevel.INFO, "Sensor getStatus");
		return this.sensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		this.sensor.update();
		//System.out.println(new Date().getTime());
		//System.out.println("Méthode appelée : public void update()");
		log.log(LogLevel.INFO, "Sensor Update");
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		log.log(LogLevel.INFO, "Sensor Value = " + sensor.getValue());
		return this.sensor.getValue();
	}

    @Override
    public double getDegCvalue() throws SensorNotActivatedException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void changeUnite() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getValueType() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
		

}

