/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.telecomnancy.sensor;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hubert52u
 */
public class ValeurARR extends Valeur{

    ISensor source;
    String name = "�C ROUNDED";
    
    public ValeurARR(ISensor s){
        source = s;
    }
    
    public double getValue() {
        
        
        try {
            return (Math.round(source.getDegCvalue() * Math.pow(10, 2))) / (Math.pow(10, 2));
            
        } catch (SensorNotActivatedException ex) {
            Logger.getLogger(ValeurARR.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
        
    }

    @Override
    public String getName() {
        return this.name;
                
    }
    
}
