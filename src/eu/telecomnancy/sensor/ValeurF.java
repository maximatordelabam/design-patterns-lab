/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.telecomnancy.sensor;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hubert52u
 */
public class ValeurF extends Valeur{

    ISensor source;
    String name = "�F";
    
    public ValeurF(ISensor s){
        source = s;
    }
    
    public double getValue() {
        
        
        try {
            return source.getDegCvalue()*1.8 + 32;
        } catch (SensorNotActivatedException ex) {
            Logger.getLogger(ValeurF.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
        
    }
    public String getName() {
        return this.name;
                
    }
    
}
