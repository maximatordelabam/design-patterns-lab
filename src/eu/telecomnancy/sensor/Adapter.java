package eu.telecomnancy.sensor;

//import java.util.ArrayList;
import java.util.Observable;

//import eu.telecomnancy.ui.IObserver;

public class Adapter extends Observable implements ISensor {
	
	//ArrayList<IObserver> listObserver = new ArrayList<IObserver>();
	
	LegacyTemperatureSensor legacySensor=new LegacyTemperatureSensor();
	
	public void on() {
		if(legacySensor.getStatus()==false){
		legacySensor.onOff();
		}
		

	}

	
	public void off() {
		if(legacySensor.getStatus()==true){
			legacySensor.onOff();
			}

	}

	
	public boolean getStatus() {
		
		return legacySensor.getStatus();
	}

	
	public void update() throws SensorNotActivatedException {
		legacySensor.onOff();
		legacySensor.onOff();
		this.setChanged();
		this.notifyObservers();

	}

	
	public double getValue() throws SensorNotActivatedException {
		
		return legacySensor.getTemperature();
	}

	
	


	//public void attach(IObserver Observer) {
		//listObserver.add(Observer);
	//}

    @Override
    public double getDegCvalue() throws SensorNotActivatedException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void changeUnite() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getValueType() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}