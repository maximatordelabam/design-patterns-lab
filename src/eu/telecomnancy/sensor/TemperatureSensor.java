package eu.telecomnancy.sensor;

import java.util.Observable;
import java.util.Random;

public class TemperatureSensor extends Observable implements ISensor {
    TemperatureSensorState state = new TemperatureSensorStateOFF();
    Valeur value = new ValeurC(this);
    double degCvalue = 0;

    @Override
    public void on() {
        state = new TemperatureSensorStateON();
        setChanged();
        notifyObservers();
    }

    @Override
    public void off() {
        state = new TemperatureSensorStateOFF();
        setChanged();
        notifyObservers();
    }

    @Override
    public boolean getStatus() {
        return state.getStatus();
    }

    @Override
    public void update() throws SensorNotActivatedException {
        state.update();
        setChanged();
        notifyObservers();
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        return state.getValue();
    }
    
    @Override
    public double getDegCvalue() throws SensorNotActivatedException {
       return state.getDegCvalue();
    }

    @Override
    public void changeUnite() throws SensorNotActivatedException{
        this.state.changeUnite();
        setChanged();
        notifyObservers();
    }
    public String getValueType() throws SensorNotActivatedException{
        
        return state.getValueType();
    }
}
