package eu.telecomnancy.sensor;

public class TemperatureSensorStateOFF extends TemperatureSensorState implements ISensor {
	
	
	
	@Override
	public void on() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void off() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean getStatus() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void update() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
		
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		throw new SensorNotActivatedException("Sensor must be activated to get its value.");
	}

    @Override
    public double getDegCvalue() throws SensorNotActivatedException{
        throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }

    @Override
    public void changeUnite() throws SensorNotActivatedException{
        throw new SensorNotActivatedException("Sensor must be activated to change system units.");
    }

    @Override
    public String getValueType() throws SensorNotActivatedException {
        throw new SensorNotActivatedException("Sensor must be activated to get system units");
    }

}
