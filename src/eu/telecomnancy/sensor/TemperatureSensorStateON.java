package eu.telecomnancy.sensor;

import java.util.Random;

public class TemperatureSensorStateON extends TemperatureSensorState {
	
	Valeur value = new ValeurARR(this);
        double degCvalue = 0;
	
	public void on() {
		// TODO Auto-generated method stub
		
	}

	public void off() {
		// TODO Auto-generated method stub
		
	}

	public boolean getStatus() {
		// TODO Auto-generated method stub
		return true;
	}


	public void update() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		this.degCvalue = (new Random()).nextDouble() * 100;
    	this.setChanged();
    	this.notifyObservers();
	}

	public double getValue(){
		// TODO Auto-generated method stub
		return value.getValue();
	}
        
        
        
        

    public double getDegCvalue() throws SensorNotActivatedException {
        return this.degCvalue;
    }

    public void changeUnite() {
        
        if(value instanceof ValeurARR){
            value = new ValeurC(this);
        }else if(value instanceof ValeurC){
            value = new ValeurF(this);
        }else{
            value = new ValeurARR(this);
        }
        
    }

    public String getValueType() {
        return value.getName();
    }
	
}
