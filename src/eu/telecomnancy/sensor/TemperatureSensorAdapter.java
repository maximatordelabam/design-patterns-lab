/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

/**
 *
 * @author hubert52u
 */
public class TemperatureSensorAdapter implements ISensor{

    
    private LegacyTemperatureSensor sensor;
    private double value;
    
    public TemperatureSensorAdapter(LegacyTemperatureSensor legacySensor){
        this.sensor = legacySensor;
        value = legacySensor.getTemperature();
    }
    
    
    public void on() {
        
        if(!sensor.getStatus()){ // If the sensor is switched off
            sensor.onOff();
        }
        
    }

    public void off() {
         if(sensor.getStatus()){ // If the sensor is switched on
            sensor.onOff();
        }
    }

    public boolean getStatus() {
        return sensor.getStatus();
    }

    
    public void update() throws SensorNotActivatedException {
        
        if(!sensor.getStatus()){
            throw new SensorNotActivatedException("Le capteur doit être allumé pour l'actualiser.");
        }else{
            sensor.onOff();
            sensor.onOff();
            value = sensor.getTemperature();
        }
        
    }

    public double getValue() throws SensorNotActivatedException {
        
        return value;
        
    }

    @Override
    public double getDegCvalue() throws SensorNotActivatedException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void changeUnite() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getValueType() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
