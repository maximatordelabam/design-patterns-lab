/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Maxime
 */
public class CommandeUpdate implements Commande{
    
    private ISensor sensor;
    
    public CommandeUpdate(ISensor s){
        
        this.sensor = s;
        
    }
    
    public ISensor execute(){
        
        try {
            sensor.update();
        } catch (SensorNotActivatedException ex) {
            Logger.getLogger(CommandeUpdate.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sensor;
        
    }
    
    
}
