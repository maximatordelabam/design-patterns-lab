package eu.telecomnancy.helpers;

import static com.sun.jmx.mbeanserver.Util.cast;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.ui.ConsoleUI;
import eu.telecomnancy.ui.MainWindow;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: charoy
 * Date: 14/11/13
 * Time: 21:47
 */
public class ReadPropertyFile {
    public Properties readFile(String filename) throws IOException {
        Properties p= new Properties();
        p.load(this.getClass().getResourceAsStream(filename));
        return p;
    }
    
    public ArrayList<ISensor> Factory(String filename) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        
        Properties p = readFile(filename);
        ArrayList<ISensor> toExecute = new ArrayList<>();
        for (String i: p.stringPropertyNames()) {
            
            Class type;
            type = cast(Class.forName(p.getProperty(i)));
            System.out.println(p.getProperty(i));
            
            toExecute.add((ISensor) type.newInstance());
            
        }
        
        return toExecute;
    
    }
    
    
    public static void main(String[] args) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        
        ReadPropertyFile rp=new ReadPropertyFile();
        
        ArrayList<ISensor> Factory = rp.Factory("/commande.properties");
        
               
        

    }
}
