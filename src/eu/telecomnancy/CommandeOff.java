/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;

/**
 *
 * @author Maxime
 */
public class CommandeOff implements Commande{
    
    private ISensor sensor;
    
    public CommandeOff(ISensor s){
        
        this.sensor = s;
        
    }
    
    public ISensor execute(){
        
        sensor.off();
        return sensor;
        
    }
    
    
}
