/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.sensor.ValeurARR;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Maxime
 */
public class CommandeSetUnitCarr implements Commande{
    
    private ISensor sensor;
    
    public CommandeSetUnitCarr(ISensor s){
        
        this.sensor = s;
        
    }
    
    public ISensor execute(){
        
        try {
            while(sensor.getValueType() != new ValeurARR(sensor).getName()){
            sensor.changeUnite();
            }
        } catch (SensorNotActivatedException ex) {
            Logger.getLogger(CommandeSetUnitCarr.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sensor;
        
    }
    
    
}
